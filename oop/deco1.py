# print("Starting")  # 1
# def make_pretty(func):  # 3
#     print("Inside make_preety")
#
#     def inner():  # 7
#         print("I got decorated")
#         func()
#     return inner  # 4
# print('Hi')
# @make_pretty  # 2
# def ordinary():
#     print("I am ordinary")
#
# print('Hello world')  # 5
# ordinary()  # 6

# def parentfunction(mainfunction):#2
#     print("Parent function")
#     def childfunction(a, b): # 6
#         print("child function")
#         print("I am going to divide", a, "and", b)
#         if b == 0:
#             print("Whoops! can not divide")
#             return
#         return mainfunction(a, b)
#     return childfunction # 3
#
# print('hi')
# @parentfunction #1 starts from here # return to here 4
# def dividetewonumber(a, b):# 7
#     print("Before dividing a/b")
#     return a/b
# res = dividetewonumber(3,4) # 5
# print(res)

###################################
# def make_pretty(func):
#     def inner():
#         print("I got decorated")
#         func()
#     return inner
#
# def ordinary():
#     print("I am ordinary")
# # let's decorate this ordinary function
# pretty = make_pretty(ordinary)
# pretty()
#################################

"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
# print('jj')
# def star(func):
#     def inner(*args, **kwargs):
#         print("-" * 30)
#         func(*args, **kwargs)
#         print("*" * 30)
#     return inner
# # def percent(func):
# #     def inner(*args, **kwargs):
# #         print("%" * 30)
# #         func(*args, **kwargs)
# #         print("%" * 30)
# #     return inner
# @star # ==> printer = star(printer)
# # in the above when start is returning inner then @star becomes inner and when in line number 70 we says printer("hello")
# #that means inner("Hello")
# # @percent
# def printer(msg):
#     print(msg)
# printer("Hello")
"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
# def star(func):
#     def inner(*args, **kwargs):
#         print("*" * 30)
#         func(*args, **kwargs)
#         print("*" * 30)
#     return inner
#
# def percent(func):
#     def inner(*args, **kwargs):
#         print("%" * 30)
#         func(*args, **kwargs)
#         print("%" * 30)
#     return inner
# print("lets debug")
#
# @star
# @percent
# def printer(msg):
#     print(msg)
# printer("Hello")


# @star
# @percent
# def printer(msg):
#     print(msg)
# is equivalent to
#
# def printer(msg):
#     print(msg)
# printer = star(percent(printer))

# from Package_practice import show, blogic
# show.show()
# blogic.add()


############################
# class test:
#     _a = "test"
#     __b = "Madan"
#     def __init__(self, aattribute):
#         self._a = aattribute
#         self.__b = aattribute
#         self.__x = "XXX"
#     def sample(self):
#         print("Sample method")
#         self.__x = "Sample"
#         return self.__x
# obj = test("vikash")
# print(obj._a)
# #print(obj._test__b) # o/p
# #obj._test__b = "Praful" #
# #print(obj._test__b) # o/p
# print(obj._a) # o/p
# print(obj.__b) # attribute erro

# from Package_practice import Show, blogic
# Show.show()
#
# from testdar import praful
# msg = praful.pythonpra()
#
# print(msg)
# import pickle
# import os
# my_list = [1,2,3]
# # with open('test.py', 'wb') as f:
# #     pickle.dump(my_list, f)
#
# # with open("test.py", "rb") as f:
# #     pickle.load(f)
#
# # with open("test.py", "rb") as f:
# #     fdata = pickle.load(f)
# # print(fdata)
# pickle_off = open ("test.py", "rb")
# emp = pickle.load(pickle_off)
# pickle_off.close()
# print(emp)
# with open("vikash.txt", "a") as f:
#     f.write(str(emp))

# def fib(n):
#     p, q = 0, 1
#     while(p < n):
#         yield p
#         p, q = q, p + q
#         try:
#             print(p,q)
#         except StopIteration as e:
#             print(e)
#
# x = fib(10)
# for i in range(8):
#     # print(x.__next__())
#     try:
#         print(x.__next__())
#     except StopIteration as SI:
#         print(SI)
# l1 = [1,2,3,4]
# l2 = [4,5,6,7,1]
# rn=0
# L = []
# for i in l1:
#     if i not in l2:
#         rn += 1
#         L.append(i)
#
#         # print(rn)
#     # else:
#     #     rn = 0
# print(rn)
# print(L)
# for i in range(6):
#     if i not in l1:
#         print()

# def factorial(n):
#     if n == 0:
#         return 1
#
#     return n * factorial(n - 1)
#
# # Driver Code
# num = 6;
# print("Factorial of", num, "is",
#       factorial(num))

# def factfun():
#     n = int(input("enter a number"))
#     fact = 1
#     if n == 0 or n == 1:
#         print("Factorial of {0} is {1}".format(n, fact))
#     elif n < 0:
#         print("Please enter a positive integer")
#     else:
#         for i in range(2,n+1):
#             fact *= i
#         print(fact)
#     return
# factfun()

class Employee:
    __pythontl = "Madan"
    _javatl = "Hari"
    dotnettl = "sagar"
    def __init__(self):
        print(self.__pythontl)
        print(self._javatl)
        print(self.dotnettl)
    def non_tech(self):
        print('Method')
obj1 = Employee()
# b = obj1._javatl
a = obj1._Employee__pythontl

print(a)
