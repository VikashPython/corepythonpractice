"""Your task is to make a function that can take any non-negative integer as an argument and return it with its digits
in descending order. Essentially, rearrange the digits to create the highest possible number.
Examples:

Input: 21445 Output: 54421

Input: 145263 Output: 654321

Input: 1254859723 Output: 9875543221 """


def number(num):
    num = list(str(num))
    num.sort(reverse=True)
    final = int(''.join(num))
    return final


# print(number(21445))
# print(number(1254859723))
# print(number(145263))
# ends here

"""concatenate list of elements"""
# 1st approach
li = [1, 2, 3, 4, 5]
s1 = ''
for i in li:
    s1 += str(i)
# print(s1)

# 2nd approach
print(int(''.join(map(str, [1, 2, 3, 4]))))


