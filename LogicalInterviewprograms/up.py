print('-----------')
x = ['ab', 'cd']
# the below end will be endless and may harm your system
# for that we have added a break statement
for i in x:
    x.append(i.upper())
    print(x)
    if len(x) > 4:
        break
print(x)

