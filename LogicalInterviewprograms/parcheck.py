# p = ']['
# p = input('Enter paranthesis')
# op = ['[', '(', '{']
# cp = [']', ')', '}']
# n = 0
# for i in p:
# 	if i in op:
# 		n += 1
# 	elif i in cp:
# 		n -= 1
# if n == 0:
#     print("Properly closed",n)
# else:
#     print("Not closed", n)

# Python3 code to Check for
# balanced parentheses in an expression
open_list = ["[", "{", "("]
close_list = ["]", "}", ")"]


# 1 : Using stack
# def check(myStr):
#     stack = []
#     for i in myStr:
#         if i in open_list:
#             stack.append(i)
#         elif i in close_list:
#             pos = close_list.index(i)
#             if (len(stack) > 0) and (open_list[pos] == stack[len(stack) - 1]):
#                 stack.pop()
#             else:
#                 return "Unbalanced"
#     if len(stack) == 0:
#         return "Balanced"
#     else:
#         return "Unbalanced"
#
#
# string = "{[]{()}}"
# print(string, "-", check(string))

# string = "[{}{})(]"
# print(string,"-", check(string))
#
# string = "((()"
# print(string,"-",check(string))

# 2 : Using queue
# Python3 code to Check for
# balanced parentheses in an expression
# def check(expression):
#     open_tup = tuple('({[')
#     close_tup = tuple(')}]')
#     map = dict(zip(open_tup, close_tup))
#     queue = []
#
#     for i in expression:
#         if i in open_tup:
#             queue.append(map[i])
#         elif i in close_tup:
#             if not queue or i != queue.pop():
#                 return "Unbalanced"
#     if not queue:
#         return "Balanced"
#     else:
#         return "Unbalanced"


# string = "{[]{()}}"
# print(string, "-", check(string))

# string = "((()"
# print(string, "-", check(string))

# 3 : Elimination based
# In every iteration, the innermost brackets get eliminated (replaced with empty string).
# If we end up with an empty string, our initial one was balanced; otherwise, not.
# def check(my_string):
#     brackets = ['()', '{}', '[]']
#     while any(x in my_string for x in brackets):
#         for br in brackets:
#             my_string = my_string.replace(br, '')
#     return not my_string
#
#
# string = "{[]{()}}"
# print(string, "-", "Balanced" if check(string) else "Unbalanced")

# def addtwonum(abc):
#     return abc + abc
#
#
# li = [1, 2, 3, 4]
# a = map(addtwonum, li)
# print(list(a))

# sorting list of strings
# but in this last string of same length will be added to sorted list
c = ['vikash', 'kumar', 'barnwal', 'z', 'a']
# d = {}
# for i in c:
#     li = len(i)
#     d[li] = i
# e = d.keys()
# f = sorted(e)
# g = []
# for i in f:
#     g.append(d[i])
# print(g)


# def sortinglist(ele):
#     ele.sort(key=len)
#     # newlst=sorted(ele, key=len)
#     # return newlst
#     return ele
#
#
# print(sortinglist(c))

# c.sort(key=len)
# print(c)

# d = sorted(c, key=len)
# print(d)

# for i in range(len(c)):
#     idx = i
#     for j in range(i + 1, len(c)):
#         if len(c[idx]) > len(c[j]):
#             idx = j
#     c[i], c[idx] = c[idx], c[i]
# print(c)

#############################
# a, b = [1, 2, 3], [4, 5, 6]
# cc = [x+y for x, y in zip(a, b)]
# # c = (x+y)
# print(cc)

# for i in range(len(c) - 1):
#     for j in range(len(c) - i - 1):
#         if len(c[j]) > len(c[j + 1]):
#             c[j], c[j + 1] = c[j + 1], c[j]
# print(c)

# li = [1, 2, 3, 5, 6, 7, 8, 9, 10]
# mising_values = [x for x in range(1, 11) if x not in li]
# print(mising_values)

# class Foo:
#     def bar(cls):
#         print("bar")
#     bar = classmethod(bar)
# Foo.bar  # <bound method classobj.bar of <class __main__.Foo at 0x00CDAC00>>
# Foo.bar()  # bar
# Foo().bar  # <bound method classobj.bar of <class __main__.Foo at 0x00CDAC00>>
# Foo().bar()  # bar

# class Show:
#     emp="Vikash"
#     @classmethod
#     def cm(cls):
#         print("class method")
#         print("EmpName:", Show.emp)
#         print(cls.emp)
#
# Show.cm()
# obj = Show()
# obj.cm()

# factory method
# from datetime import date
#
# # random Person
# class Person:
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
#     @classmethod
#     def fromBirthYear(cls, name, birthYear):
#         return cls(name, date.today().year - birthYear)
#
#     def display(self):
#         print(self.name + "'s age is: " + str(self.age))
#
# person = Person('Adam', 19)
# person.display()
#
# person1 = Person.fromBirthYear('John',  1985)
# person1.display()
#
# class C(object):
#     def __init__(self):
#         self._x = None
#     def getx(self):
#         return self._x
#     def setx(self, value):
#         self._x = value
#     def delx(self):
#         del self._x
#     x = property(getx, setx, delx, "I'm the 'x' property.")
# obj = C()
# print(obj.x)

# Basic method of setting and getting attributes in Python
# class Celsius:
#     def __init__(self, temperature=0):
#         self.temperature = temperature
#
#     def to_fahrenheit(self):
#         return (self.temperature * 1.8) + 32
#
#
# # Create a new object
# human = Celsius()
#
# # Set the temperature
# human.temperature = 37
#
# # Get the temperature attribute
# print(human.temperature)
#
# # Get the to_fahrenheit method
# print(human.to_fahrenheit())

# Making Getters and Setter methods
# class Celsius:
#     def __init__(self, temperature=0):
#         self.set_temperature(temperature)
#
#     def to_fahrenheit(self):
#         return (self.get_temperature() * 1.8) + 32
#
#     # getter method
#     def get_temperature(self):
#         return self._temperature
#
#     # setter method
#     def set_temperature(self, value):
#         if value < -273.15:
#             raise ValueError("Temperature below -273.15 is not possible.")
#         self._temperature = value


# # Create a new object, set_temperature() internally called by __init__
# human = Celsius(37)
# # Get the temperature attribute via a getter
# print(human.get_temperature())

# # Get the to_fahrenheit method, get_temperature() called by the method itself
# print(human.to_fahrenheit())

# # new constraint implementation
# human.set_temperature(-300)

# # Get the to_fahreheit method
# print(human.to_fahrenheit())
#####################################################
# using property class
# class Celsius:
#     def __init__(self, temperature=0):
#         self.temperature = temperature

#     def to_fahrenheit(self):
#         return (self.temperature * 1.8) + 32

#     # getter
#     def get_temperature(self):
#         print("Getting value...")
#         return self._temperature

#     # setter
#     def set_temperature(self, value):
#         print("Setting value...")
#         if value < -273.15:
#             raise ValueError("Temperature below -273.15 is not possible")
#         self._temperature = value

#     # creating a property object
#     temperature = property(get_temperature, set_temperature)
#
#
# human = Celsius(37)
# print(human.temperature)
# print(human.to_fahrenheit())
# human.temperature = -300
# class Dates:
#     def __init__(self, date):
#         self.date = date
#
#     def getDate(self):
#         return self.date
#
#     @staticmethod
#     def toDashDate(date):
#         return date.replace("/", "-")


# date = Dates("15-12-2016")
# dateFromDB = "15/12/2016"
# dateWithDash = Dates.toDashDate(dateFromDB)
#
# if (date.getDate() == dateWithDash):
#     print("Equal")
# else:
#     print

# class Foo:
#     def __init__(self, x):
#         self.x=x
#         print(self.x)
# f = Foo(10)
# print(f.x)
#
# setattr(f,'x', 20) # xyv
# print('After setattr')
# print(f.x)
# import time
#
# # starting time
# startt = time.time()
#
# # program body starts
# start = int(input("Enter lower range: "))
# end = int(input("Enter upper range: "))

# for val in range(start, end + 1):
#     if val > 1:
#         for n in range(2, val//2 + 2):
#             if (val % n) == 0:
#                 break
#             else:
#                 if n == val//2 + 1:
#                     # pass
#                     print(val)
#
# # sleeping for 1 sec to get 10 sec runtime
# # time.sleep(1)
# # program body ends
#
# # end time
# end = time.time()

# total time taken
# print(f"Runtime of the program is {end - startt}")
# Starting_Number = int(input("Enter starting Number"))
# Ending_Number = int(input('Enter End Number'))
# for i in range(Starting_Number, Ending_Number+1):
#     if i >1:
#         for j in range(2, i//2+1):
#             if i % j == 0:
#                 break
#             else:
#                 pass

# Python program to print all
# prime number in an interval

start = 2
end = 25

# for i in range(start, end + 1):
#     if i == 2:
#         print(i)
#     if i > 1:
#         for j in range(2, i//2 + 2):
#             if (i % j) == 0:
#                 break
#             else:
#                 print(i)

for val in range(start, end + 1):
    if val > 1:
        for n in range(2, val // 2 + 2):
            if (val % n) == 0:
                break
        else:
            # print(n, val)
            pass
# #prime number interval#########
# for val in range(start, end + 1):
#
#     if val > 1:
#         if val ==2:
#             print(val)
#         else:
#             for n in range(2, val // 2 + 2):
#                 if (val % n) == 0:
#                     break
#                 elif n == val//2+1:
#                     print(val)
# ####to check a number is prime or not######
# n = int(input('Enter a number'))
# if n>1:
#     if n == 2:
#         print(n)
#     else:
#         for i in range(2,n//2+2):
#             if n%i == 0:
#                 print(f"{n} is not a prime number")
#                 break
#             elif i == n//2+1:
#                 print(f"{n} is prime number")

# Function for nth Fibonacci number

# def Fibonacci(n):
# 	if n<0:
# 		print("Incorrect input")
# 	# First Fibonacci number is 0
# 	elif n==1:
# 		return 0
# 	# Second Fibonacci number is 1
# 	elif n==2:
# 		return 1
# 	else:
# 		return Fibonacci(n-1)+Fibonacci(n-2)
#
# # Driver Program
#
# print(Fibonacci(4))

# Function for nth fibonacci number - Dynamic Programing
# Taking 1st two fibonacci nubers as 0 and 1

# FibArray = [0,1]
#
# def fibonacci(n):
# 	if n<0:
# 		print("Incorrect input")
# 	elif n<=len(FibArray):
# 		return FibArray[n-1]
# 	else:
# 		temp_fib = fibonacci(n-1)+fibonacci(n-2)
# 		# FibArray.append(temp_fib)
# 		return temp_fib
#
# # Driver Program
#
# print(fibonacci(9))
# {'a':[("absent",1), ('present':1)],'b':[("absent",1), ('present':1)]}
# l=[['vikash','absent'], ['madan',"absent"], ['vikash', 'present'], ['madan', 'present'], ['vikash','None']]
#
# # d = {'vikash':{'absent':1, 'present':1}, 'madan':{'absent':1, 'present':1}}
#
# d={}
# for x in l:
#     if x[0] not in d:
#         d[x[0]] = {}
#     if x[1] not in d[x[0]]:
#         d[x[0]][x[1]] = 1
#     else:
#         d[x[0]][x[1]] += 1
#
# print(d)

# # Python 3 code to find sum
# # of elements in given array
# def _sum(arr, n):
#     # return sum using sum
#     # inbuilt sum() function
#     return (sum(arr))
#
#
# # driver function
# arr = []
# # input values to list
# arr = [12, 3, 4, 15]
#
# # calculating length of array
# n = len(arr)
#
# ans = _sum(arr, n)

# display sum
# print('Sum of the array is ', ans)
# Python program for reversal algorithm of array rotation
# import time
# start_time = time.time()
# # # Function to reverse arr[] from index start to end
# # def rverseArray(arr, start, end):
# # 	while (start < end):
# # 		temp = arr[start]
# # 		arr[start] = arr[end]
# # 		arr[end] = temp
# # 		start += 1
# # 		end = end-1
# #
# # # Function to left rotate arr[] of size n by d
# # def leftRotate(arr, d):
# # 	n = len(arr)
# # 	rverseArray(arr, 0, d-1)
# # 	rverseArray(arr, d, n-1)
# # 	rverseArray(arr, 0, n-1)
# #
# # # Function to print an array
# # def printArray(arr):
# # 	for i in range(0, len(arr)):
# # 		print (arr[i])
# #
# # # Driver function to test above functions
# # arr = [1, 2, 3, 4, 5, 6, 7]
# # leftRotate(arr, 2) # Rotate array by 2
# # end_time = time.time()
# # printArray(arr)
#
#
#
#
# l = [1,2,3,4,5,6]
# n = int(input("Enter number of element by whihc it should left rotate" ))
# for i in range(0,n):
#     l.append(l[i])
# del l[0:n]
# print(l)

# # l = [1,2,3,4,5,6]
# # n = int(input("Enter number of element by whihc it should left rotate" ))
# # for i in range(0,n):
# #     l.append(l[i])
# # del l[0:n]
# # print(l)
# end_time = time.time()
#
# print(end_time-start_time)
# Python3 program to swap first
# and last element of a list

# Swap function
# li = ['a','a', 'b', 'c','d', 'b', 'c','e', 'a']
# l = [(), ('vikash', 'barnwal'), (), ('sagar', 'singh'), (), ('', '')]
# c = 0
# for i in l:
#     if i == ():
#         c += 1
# print(c)

# def Remove(tuples):
# 	tuples = [t for t in tuples if t]
# 	return tuples
#
# # Driver Code
# tuples = [(), ('ram','15','8'), (), ('laxman', 'sita'),
# 		('krishna', 'akbar', '45'), ('',''),()]
# print(Remove(tuples))

# Input : list = [10, 20, 30, 40, 50]
# Output : [10, 30, 60, 100, 150]
# import time
# st = time.time()

# vi = ['geeks', 'for', 'geeks', 'like','geeky','nerdy', 'geek', 'love','questions','words', 'life']
# l=[]
# n = 3
# for i in range(0, len(vi),n):
#     l.extend([vi[i:n+i]])
# print(l)
# l = [1, 2, 3, 4, 3, 1]
# li = []
# count = 0
# n = int(input("enter number"))
# while l:
#     li.append(l[:n])
#     for x in li[count]:
#         l.remove(x)
#     count += 1
# print(li)

# list1 = ["a", "b", "c", "d", "e", "f", "g", "h", "i"]
# list2 = [ 0,   1,   1,    0,   1,   2,   2,   0,   1]
# zp = sorted(zip(list2, list1))
# a = [z for v,z in zp]
# print(a)

# l="ojas123inno"
# lidigit = []
# for i in l:
#     if i.isdigit():
#         lidigit.append(int(i))
# print(lidigit)
# for i in l:
#     if i in lidigit:
#         l.replace(i,'@')
# print(l)

# newstr =""
# for i in l:
#     if i.isdigit():
#         lidigit.append(int(i))
#     else:
#         newstr = newstr+i
# print(newstr)
# print(lidigit)

# Python | Check if a Substring is Present in a Given String

# s1, s2 = "geekkkk", "geekkkk geeks for of "
# print(s1 in s2)
#######################
# if s1.find(s2):
#     print('Present')
# else:
#     print('Not Present')
#########################
# if s2.count(s1) > 0:
#     print('Yes')
# else:
#     print('No')
# import re
# if re.match(s1, s2):
#     print(re.search(s1, s2))
#     print('Yes')
# else:
#     print('No')
# def check(string):
#     # string = string.replace(' ', '')
#     # print(string)
#     string = string.lower()
#     vowel = [string.count('a'), string.count('e'), string.count('i'), string.count('o'), string.count('u')]
#     print(vowel)
#     # If 0 is present int vowel count array
#     if vowel.count(0) > 0:
#         print(vowel.count(0))
#         return ('not accepted')
#     else:
#         return ('accepted')
#
#
# if __name__ == "__main__":
#     string = "SEEuoaL"
#
#     print(check(string))

# def check(string):
#     if len(set(string).intersection("AEIOUaeiou"))>=5:
#         return ('accepted')
#     else:
#         return ("not accepted")
#
# #Driver code
# if __name__=="__main__":
#     string="geeksforgeeks"
#     print(check(string))

# str1 = 'aabcddekll12@'
# str2 = 'bb2211@55k'
# c=0
# for i in str1:
#     if i in str2:
#         c += 1
# print(c)
#############
# s1 = set(str1)
# s2 = set(str2)
# mc = s1 & s2
# print(len(mc))
#############
# import re
# c = 0
# for i in str1:
# 	if re.search(i,str2):
# 		c=c+1
# print("No. of matching characters are ", c)
#################################
# Remove all duplicates from a given string in Python
# str="geeksforgeeks"
# print(set(str))
# print("".join(set(str)))
# from collections import OrderedDict
# print("".join(OrderedDict.fromkeys(str)))

# def removeDuplicate(str):
#     s = set(str)
#     s = "".join(s)
#     print("Without Order:", s)
#     t = ""
#     for i in str:
#         if (i in t):
#             pass
#         else:
#             t = t + i
#     print("With Order:", t)
# str = "geeksforgeeks"
# removeDuplicate(str)

# import string
# import random
# possibleCharacters = string.ascii_lowercase + string.digits + string.ascii_uppercase + ' ., !?;:'
# t = "geek"
# # t = input(str("Enter your target text: "))
# attemptThis = ''.join(random.choice(possibleCharacters)for i in range(len(t)))
# # print('---------------')
# # print(attemptThis)
# attemptNext = ''
#
# completed = False
# iteration = 0
#
# # Iterate while completed is false
# while completed == False:
#     print(attemptThis)
#
#     attemptNext = ''
#     completed = True
#
#     # Fix the index if matches with
#     # the strings to be generated
#     for i in range(len(t)):
#         if attemptThis[i] != t[i]:
#             completed = False
#             attemptNext += random.choice(possibleCharacters)
#         else:
#             attemptNext += t[i]
#
#         # increment the iteration
#     iteration += 1
#     attemptThis = attemptNext
#
# print("Target matched after " +
#       str(iteration) + " iterations")

# Scenario We strongly encourage you to play with the code we've written for you, and make some (maybe even
# destructive) amendments. Feel free to modify any part of the code, but there is one condition - learn from your
# mistakes and draw your own conclusions.
#
# Try to:
#
# minimize the number of print() function invocations by inserting the \n sequence into the strings make the arrow
# twice as large (but keep the proportions) duplicate the arrow, placing both arrows side by side; note: a string may
# be multiplied by using the following trick: "string" * 2 will produce "stringstring" (we'll tell you more about it
# soon) remove any of the quotes, and look carefully at Python's response; pay attention to where Python sees an
# error - is this the place where the error really exists? do the same with some of the parentheses; change any of
# the print words into something else, differing only in case (e.g., Print) - what happens now? replace some of the
# quotes with apostrophes; watch what happens carefully.
# print("    *")
# print("   * *")
# print("  *   *")
# print(" *     *")
# print("***   ***")
# print("  *   *")
# print("  *   *")
# print("  *****")

# class A:
#     A = 1
#     def __init__(self):
#         pass
# a = A(1)
# print(hasattr(a, 'A'))

# class A:
#     pass
#
# class B(A):
#     pass
# class C(B):
#     pass
# print(issubclass(A, C))


# class I:
#     def __init__(self):
#         self.s = 'abc'
#         self.i = 0
#     def __iter__(self):
#         return self
#     def __next__(self):
#         if self.i == len(self.s):
#             raise StopIteration
#         v = self.s[self.i]
#         self.i += 1
#         return v
# for x in I():
#     print(x, end='')
