import sqlite3 as sql
conn = sql.connect("anujbank.db")
curs = conn.cursor()
def createaccount():
    try:
        name = input("Enter customer name:")
        if name.isalpha() != True:
            raise ValueError("name should have alphabate only")
        try:
            bal = float(input("enter opening balance:"))
        except ValueError:
            print("balance must be digit only")
        status = input("enter account status(active/deactive):")
        if status.isalpha() != True:
            raise ValueError("status should be alphabetic character only")
        pin = int(input("Enter PIN:"))
        curs.execute("insert into user_detail values(?,?,?,?,?)", (None, name, bal, status, pin))
        conn.commit()
        print("account created succesfully")
    except ValueError as ve:
        print(ve)
        print("thanks")
    except UnboundLocalError:
        print("give proper value first")
def deleteaccount():
    try:
        acc_no = int(input("Enter account no :"))
        curs.execute("delete from user_detail where account_no = ? ", (acc_no,))
        conn.commit()
        print(acc_no, "deleted")
    except ValueError as ve:
        print("account no must be integer only")
        print("thanks")
def changestatus():
    try:
        acc_no = int(input("Enter account no:"))
        curs.execute("select status from user_detail where account_no = ?", (acc_no,))
        res = curs.fetchone()
        for x in res:
            print("current status:", x)
        new_status = input("Enter new status:")
        if new_status.isalpha() != True:
            raise ValueError("status should be ACTIVE/DEACTIVE")
        curs.execute("update user_detail set status = ? where account_no = ?", (new_status, acc_no,))
        conn.commit()
        print("status updated")
    except ValueError as ve:
        print(ve)
def viewall():
    curs.execute("select customer_name,account_no,balance,status from user_detail")
    res = curs.fetchall()
    for x in res:
        print("Account holder Name:",x[0])
        print("Account no:",x[1])
        print("Balance:",x[2])
        print("Status:",x[3])
        print("------------------")
"""" user functions"""
def viewbalance(acc_no):
    curs.execute("select balance from user_detail where account_no = ?",(acc_no,))
    res = curs.fetchall()
    for x in res:
        return x[0]
def cashwithdrwal(acc_no):
    res = viewbalance(acc_no)
    amt = int(input("Enter amount:"))
    if amt <= 10000:
        if amt % 100 == 0:
            if amt <= res:
                upd_bal = res - amt
                curs.execute("update user_detail set balance = ? where account_no = ?",(upd_bal,acc_no,))
                conn.commit()
                print("collect cash",amt)
            else:
                print("insufficent amount")
        else:
            print("amount should be multiple of 100")
    else:
        print("amount should not exceed 10,000")
def cashdeposit(acc_no):
    print("Cash deposit")
    res = viewbalance(acc_no)
    amt = int(input("Enter amount"))
    up_bal = res + amt
    curs.execute("update user_detail set balance = ? where account_no = ?",(up_bal,acc_no,))
    conn.commit()
    print("cash deposited")
def change_pin(acc_no):
    pn = int(input("Enter your pin:"))
    curs.execute("select pin from user_detail where account_no = ?",(acc_no,))
    res_p = curs.fetchall()
    for x in res_p:
        if pn == x[0]:
            npin = int(input("Enter new pin:"))
            nnpin = int(input("Enter Pin again:"))
            if npin == nnpin:
                curs.execute("update user_detail set pin = ? where account_no = ?",(npin,acc_no,))
                conn.commit()
            else:
                print("entered pin not match")
        else:
            print("invalid pin")
def admin():
    pas = input("Enter password:")
    if pas == "admin":
        print("Welcome admin")
        print("1)create account")
        print("2)delete account")
        print("3)change status")
        print("4) view all account")
        opt2 = int(input("choose option:"))
        if opt2 == 1:
            createaccount()
        elif opt2 == 2:
            deleteaccount()
        elif opt2 == 3:
            changestatus()
        elif opt2 == 4:
            viewall()
        else:
            print("invalid option")
    else:
        print("invalid password")
def userf():
    acc_no = int(input("Enter account no:"))
    pin = int(input("Enter PIN:"))
    curs.execute("select pin,customer_name from user_detail where account_no = ?", (acc_no,))
    res = curs.fetchall()
    for x in res:
        if pin == x[0]:
            print("Welcome MR/MISS.", x[1])
            print("1) view balance")
            print("2) cash withdrawl")
            print("3) cash deposit")
            print("4) change pin")
            opt3 = int(input("choose option:"))
            if opt3 == 1:
                res = viewbalance(acc_no)
                print("Balance:", res)

            elif opt3 == 2:
                cashwithdrwal(acc_no)
            elif opt3 == 3:
                cashdeposit(acc_no)
            elif opt3 == 4:
                change_pin(acc_no)
            else:
                print("invalid option")
        else:
            print("invalid password")
def main():
    print("1) Admin")
    print("2) Customer")
    opt = int(input("choose one option:"))
    if opt == 1:
       admin()
    elif opt == 2:
        userf()
    else:
        print("invalid option")
while True:
    main()
    opt = int(input("press 1 to continue"))
    if opt != 1:
        break
main()