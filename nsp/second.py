class Employee:
    comp_name = "vikash"
    comp_cno = 1234567890

    @staticmethod
    def displayCompInfo():
        print(Employee.comp_name)
        print(Employee.comp_cno)

Employee.displayCompInfo()