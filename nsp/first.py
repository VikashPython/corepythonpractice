print(' My First Dictionary')
print('Below is copy function')
d1={101:'vikash',102:'rakesh',103:'mukesh',104:'ramesh'}
d2=d1.copy()
print(d2)

print('Below is copy function')
d3=d1.get(104)
print(d3)

print('below is item function')
d4=d1.items()
print(d4)

print('Below is keys function')
d5=d1.keys()
print(d5)
print('Below is pop function')
d6=d1.pop(103)
print(d6)
print(d1)

print('Below is popitem function')
d7=d1.popitem()
print(d7)
print(d1)

print('Below is setdefault function')
d8=d1.setdefault(101)
d9=d1.setdefault(10000)
print(d8)
print(d1)
print(d9)

print('Below is values function')
d10=d1.values()
print(d10)
print(d1)

